A year after India and Australia signed the civilian nuclear cooperation agreement during then Australian PM Tony Abbott’s visit to New Delhi, his successor, Australian PM Malcolm Turnbull told PM Narendra Modi Sunday that procedure for the pact has been completed and can now be implemented. This opens doors for the much-needed uranium supply for India’s nuclear reactors.

After the meeting, Ministry of External Affairs spokesperson Vikas Swarup described it as “milestone achievement” as the two PMs announced completion of procedures for the agreement.

“The PM thanked the Australian PM and said the nuclear agreement is a milestone and a source of trust and confidence. With the completion of procedures, including administrative arrangements, the Civil Nuclear Agreement will now enter into force,” he said.

This was Modi’s first meeting with Turnbull after he took over. Sources described the meeting to be “very constructive” and pointed out to the result-oriented conversation in their first-ever bilateral meeting.

It may be recalled that former Australian PM Julia Gillard paid a state visit to India in October 2012. The decision of Australian government to supply uranium to India was taken during her time and on September 5, 2014, India and Australia signed a MoU for “Cooperation in the Peaceful Uses of Nuclear Energy” during Abbott’s visit.

The significant part of the civil nuclear cooperation agreement was that Australia agreed to become ‘a long-term reliable supplier of uranium to India’.

Right now, Australia is considered to have the largest reserve of recoverable uranium. World Nuclear Association and Australian government say that Australia ranks third in terms of production of uranium. Kazakhstan and Canada produce more than Australia.

If it opens new mines, which it plans to, its production may increase, and it might become the top producer in a few years. Australia produces uranium basically to export as it does not operate nuclear power plants. So, Australia may become the most important uranium supplier to India.

The uranium supply is critical to India’s plans for the nuclear energy expansion. The current nuclear expansion plan needs uranium and India has concluded agreements with Canada and Kazakhstan among others.